---
layout: handbook-page-toc
title: "Compliance Information"
description: "This page offers a single source related to compliance information."
---
Please note that all links are GitLab-internal only.

## Trading Window Status
- GitLab Team Members can view the current trading window status via the [Trading Window Status Issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12780).


## Insider Trading Policy and Insider Trading Policy FAQ
- [Insider Trading Policy](https://drive.google.com/file/d/12H-H43vIf15fWADZDEf3FH2jneMmiLDH/view?usp=sharing)
- [Insider Trading Policy FAQ](https://docs.google.com/document/d/1vKFiYuieDQtKmrak-aAB2dTT3B3Q-aU4DtbcRm27X4U/edit?usp=sharing)


## Designated Insiders 
- [Designated Insider Pre-Clearance Process & FAQ](https://docs.google.com/document/d/1mcBtnfGbv4jSsJUklMQYyj2052MBHe4Lf9RkE-B9yvA/edit?usp=sharing)
- [Legal Office Hours: Designated Insider Agenda & Notes](https://docs.google.com/document/d/128ufQKKCkVXJgd0tEp81XQnhr5X1piGpZtxrXJ3iyzk/edit#heading=h.hnzbmxmoglh5)
- [Designated Insider Slack Communication](https://docs.google.com/document/d/1y7m1tOISRQjWAZKoxzii6SPxMknkexN6JQxNi4VMvPU/edit) _Note:_ This was the inital communication posted to `#designated-insiders`
- [Designated Insider: Pre-Clearance Form Demo](https://www.youtube.com/watch?v=7vas2sNjYxg)

